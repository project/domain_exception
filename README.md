# domain_exception



-- SUMMARY --

A Drupal 8 module to add a exception to exclude specific domains from the field content and block contents. Specific domains will treat as absolute URLs and will need to be replace with relative URLs using hook_entity_type_alter().

For a full description of the module, visit the project page:

To submit bug reports and feature suggestions, or to track changes:


-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/1897420 for further information.


-- CONFIGURATION --

* Add absolute URLs or domain configuration in Administration » Configuration »
 System » Domain Exception.
 
  - Add Validation message and Absoulte URLs configuration to add in system.
  

-- CONTACT --

Current maintainers:
* DevTeam Backstage (backstage) - https://www.drupal.org/user/3646190
