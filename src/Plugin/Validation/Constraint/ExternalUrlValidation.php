<?php

namespace Drupal\domain_exception\Plugin\Validation\Constraint;
use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value is a absolute URL.
 *
 * @Constraint(
 *   id = "ExternalUrlValidation",
 *   label = @Translation("External Absolute URL validation", context = "Validation"),
 *   type = "string"
 * )
 */
class ExternalUrlValidation extends Constraint {
  // The message that will be shown if the value is an absolute URL.
  public $host_urls='';
  public $validation_message='';
}