<?php

namespace Drupal\domain_exception\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueInteger constraint.
 */
class ExternalUrlValidationValidator extends ConstraintValidator {
	
  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
	 
	foreach ($items as $item) {
      // Next check if the value is not absolute URL.
	  if (!$this->absoluteURLValidation($item->value,$constraint->host_urls)) {
	   $this->context->addViolation($constraint->validation_message, ['%field_name' => $item->getFieldDefinition()->label()]);
      }
    }
  }

  /**
   * Absolute URL validation
   *
   * @param string $value
   */
  private function absoluteURLValidation($value,$host_urls) {
	// Get All links from the content
	$list = array();
	//if(preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $value, $links, PREG_PATTERN_ORDER)){
	if(!is_array($value) && preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $value, $links)){ // This will check href on whatever place placed in anchor tag.
		foreach ($links[2] as $key => $value) {
			$list[] = $value;
		}
	}
	
	if(isset($list) && !empty($list)){
	  $preparedUrls = $this->generate_url_array($list);
	}
	if(!empty($preparedUrls))
	{
		//intersect will check mentioned urls are exist in array or not
		$host_urls = trim(preg_replace('/\s\s+/', ' ', $host_urls));
		$host_url_array = explode(' ',$host_urls);
		$flag = array_intersect($host_url_array,$preparedUrls);
		if(count($flag)>0){
			//throwing error for Absolute url.
			return 0;
		}
		else{
			return 1;
		}
	}
	else
	{
	 return 1;
	}
}
/**
   * Generate valid URLs
   *
   * @param Array
   */
function generate_url_array($list){
	$preparedUrls = array();
	if(isset($list) && !empty($list)){
		foreach ($list as $url) {
			$item = parse_url($url);
			if(isset($item['scheme']) && isset($item['host'])){
				if($item['scheme']=='http')
				{
				 $preparedUrls[] = $item['scheme'].'s://'.$item['host'].'/'; //store absolute urls in array.
				}
				else
				{
				 $preparedUrls[] = $item['scheme'].'://'.$item['host'].'/'; //store absolute urls in array.
				}
			}
		}
	}	
	return $preparedUrls;
 }
}