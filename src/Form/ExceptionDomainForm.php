<?php  
/**  
 * @file  
 * Contains Drupal\domain_exception\Form\ExceptionDomainForm.  
 */  
namespace Drupal\domain_exception\Form;  
use Drupal\Core\Form\ConfigFormBase;  
use Drupal\Core\Form\FormStateInterface;  

class ExceptionDomainForm extends ConfigFormBase {  
/**  
* {@inheritdoc}  
*/  
  protected function getEditableConfigNames() {  
    return [  
      'domain_exception.settings',  
    ];  
  }  

/**  
* {@inheritdoc}  
*/  
  public function getFormId() {  
    return 'domain_exception_form';  
  }  

/**  
* {@inheritdoc}  
*/  
  public function buildForm(array $form, FormStateInterface $form_state) {  
    $config = $this->config('domain_exception.settings');  

    $form['validation_message'] = [  
      '#type' => 'textarea',  
      '#title' => $this->t('Validation message to show'),  
      '#description' => $this->t('Use the <strong>%field_name</strong> token in message to display field name. <br> <strong>Example Message:</strong> The field is using an Absolute URL. Only use Relative URLs when linking to content within the Webster public or private website, this will eliminate broken referencing as we migrate content through the environments regularly.'),  
      '#default_value' => $config->get('domain_exception.validation_message'),  
    ]; 

	$form['input_absolute_url'] = [  
      '#type' => 'textarea',  
      '#title' => $this->t('Enter the absolute Urls or domains'),  
      '#description' => $this->t('The absolute urls or domains must be of either http or https protocols and multiple domains or urls need to be added in seperate line followed by forward slash.<br> <strong>Example:</strong> "https://example.com/"'),  
      '#default_value' => $config->get('domain_exception.input_absolute_url'),  
    ];   

    return parent::buildForm($form, $form_state);  
  }  
 /**
 * {@inheritdoc}
 */
	public function validateForm(array &$form, FormStateInterface $form_state) {
	  if ($form_state->getValue('validation_message')=='') {
		$form_state->setErrorByName('validation_message', $this->t('The Validation message to show field is required'));
	  }
	  if($form_state->getValue('input_absolute_url')==''){
	  	$form_state->setErrorByName('input_absolute_url', $this->t('The Absoulte Urls or domain field is required'));
	  }
	}
  
  /**  
   * {@inheritdoc}  
   */  
  public function submitForm(array &$form, FormStateInterface $form_state) {  
    parent::submitForm($form, $form_state);  

    $this->config('domain_exception.settings')  
      ->set('domain_exception.validation_message', $form_state->getValue('validation_message'))  
      ->set('domain_exception.input_absolute_url', $form_state->getValue('input_absolute_url'))  
      ->save();  
  } 
}  
